#include "ccolumndelegate.h"

#include <QTextEdit>
#include <QSpinBox>

CColumnDelegate::CColumnDelegate(QObject *parent, INDEXES colindex) :
    QStyledItemDelegate(parent), columnindex(colindex)
{
}

void CColumnDelegate::setEditorData(QWidget *editor_widg, const QModelIndex &index) const
{
    if (columnindex==ContentIndex)
    {
        QTextEdit* editor = qobject_cast<QTextEdit*>(editor_widg);
        editor->setPlainText(index.data().toString());
    }
    else
        return QStyledItemDelegate::setEditorData(editor_widg,index);
}

void CColumnDelegate::setModelData(QWidget *editor_widg, QAbstractItemModel *model,const QModelIndex &index) const
{
    if (columnindex==ContentIndex)
    {
        QTextEdit* editor = qobject_cast<QTextEdit*>(editor_widg);
        model->setData(index,qVariantFromValue(editor->toPlainText()));
    }
    else
        return QStyledItemDelegate::setModelData(editor_widg,model,index);
}

QWidget *CColumnDelegate::createEditor(QWidget *parent,const QStyleOptionViewItem &option,const QModelIndex &index) const
{
    if (columnindex==ContentIndex)
    {
        QTextEdit* editor = new QTextEdit(parent);
        // disable rich text, we just need multilined QLineEdit functionality :-)
        editor->setAcceptRichText(false);
        return editor;
    }
    else
        if (columnindex==FullIndex)
        {
            // for 'full' column(data type: boolean) we should use SpinBox editor
            QSpinBox* editor = new QSpinBox(parent);
            editor->setRange(0,1);
            return editor;
        }
        else
            return QStyledItemDelegate::createEditor(parent,option,index);
}

QSize CColumnDelegate::sizeHint(const QStyleOptionViewItem& option,const QModelIndex& index) const
{
    // apply size hints only for 'content' and 'comment' columns
    if (columnindex==ContentIndex or columnindex==CommentIndex)
    {
        QSize orig_size=QStyledItemDelegate::sizeHint(option,index);
        // additional 20 pixels(approx. 1 line) to height for better user expirience :-)
        return QSize(orig_size.width(),orig_size.height()+20);
    }
    else
        return QStyledItemDelegate::sizeHint(option,index);
}
