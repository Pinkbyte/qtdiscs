#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class CDatabase;
class CSettings;

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
signals:
    void mainWindowInitialized();
    void dumpSaveFileName(const QString& filename);
    void dumpRestoreFileName(const QString& filename);
public:
    bool init();
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void filtering(bool buttonState);
    void addRecord();
    void deleteRecord();
    void aboutQt();
    void aboutProgram();
    void afterConstructor();
    void databaseError(const QString& errorMessage);
    void executeDumpSaveDialog();
    void executeDumpRestoreDialog();
    void notImplemented();
private:
    CDatabase* db;
    CSettings* settings;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
