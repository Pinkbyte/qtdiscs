#ifndef CSETTINGS_H
#define CSETTINGS_H

#include <QSettings>

class CSettings : protected QSettings
{
    Q_OBJECT
public:
    CSettings();
    QString getDBType(); // returns database type(or 'QSQLITE', if no type is set)
    QString getDBHost(); // returns database server host(or 'localhost', if no host is set)
    QString getDBName(); // returns database name(or 'discs.sqlite', if no name is set)
    QString getDBUser(); // returns username for database(or 'discs', if no username is set)
    QString getDBPassword(); // returns user password for database(or 'discs', if no password is set)

signals:

public slots:

};

#endif // CSETTINGS_H
