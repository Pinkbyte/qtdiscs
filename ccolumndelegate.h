#ifndef CCOLUMNDELEGATE_H
#define CCOLUMNDELEGATE_H

#include <QStyledItemDelegate>

class CColumnDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    enum INDEXES{
        NumIndex=10,
        ContentIndex=20,
        CommentIndex=30,
        FullIndex=40,
        ErrorIndex=9999
    };

    explicit CColumnDelegate(QObject *parent=0, INDEXES columnindex=ErrorIndex);
    QWidget* createEditor(QWidget *parent,const QStyleOptionViewItem &option,const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    QSize sizeHint( const QStyleOptionViewItem & option, const QModelIndex & index ) const;
signals:

private:
    int columnindex;
};

#endif // CCOLUMNDELEGATE_H
