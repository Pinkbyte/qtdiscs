#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ccolumndelegate.h"
#include "csettings.h"
#include "cdatabase.h"

#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QFileDialog>
#include <QMessageBox>
#include <QIcon>

void MainWindow::filtering(bool buttonState)
{
    // method for filtering selected records
    if (ui->filterText->text()=="")
    {
        ui->filterButton->setChecked(false);
        buttonState=false;
    }
    QString filterPredicate="";
    if (buttonState)
        filterPredicate="content LIKE '%"+ui->filterText->text()+"%'";
    db->setModelFilter(filterPredicate);
    ui->tableview->resizeRowsToContents();
}

void MainWindow::addRecord()
{
    // method for adding new record to DB
    db->insertRowToModel();
    ui->tableview->scrollToTop();
    ui->tableview->scrollToBottom();
}

void MainWindow::deleteRecord()
{
    // method for deleting record from DB
    // num - number of record which will be deleted
    int num=ui->tableview->currentIndex().row();
    const QMessageBox::StandardButton& result=QMessageBox::information(this,tr("Information"),QString("Вы уверены, что хотите удалить строку №%1").arg(num+1),QMessageBox::Yes,QMessageBox::No);
    if (result==QMessageBox::Yes)
    {
        db->removeRowFromModel(num);
        ui->tableview->resizeRowsToContents();
    }
}

void MainWindow::aboutQt()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::aboutProgram()
{
    QMessageBox mbox(this);
    QStringList message;
    message<<tr("QtDiscs","program name");
    message<<tr("Author: Sergey Popov(Pinkbyte)");
    message<<QString(tr("Compiled with Qt %1")).arg(QT_VERSION_STR);
    message<<QString(tr("Running on Qt %1")).arg(qVersion());
    mbox.setWindowTitle(tr("QtDiscs"));
    mbox.setText(message.join("\n"));
    QIcon myicon=QIcon(":/images/media-optical-dvd-video.png");
    mbox.setIconPixmap(myicon.pixmap(QSize(64,64)));
    mbox.exec();
}

void MainWindow::executeDumpSaveDialog()
{
    QFileDialog fd(this, tr("Save SQL dump"), QDir::currentPath().append("/discs.sql"), "SQL Dump files (*.sql)");
    fd.setAcceptMode(QFileDialog::AcceptSave);
    if (fd.exec())
    {
        // if user chose file name, send it to begin dump process
        emit dumpSaveFileName(fd.selectedFiles().first());
    }
}

void MainWindow::executeDumpRestoreDialog()
{
    QFileDialog fd(this, tr("Restore SQL dump"), QDir::currentPath().append("/ "), "SQL Dump files (*.sql)");
    fd.setAcceptMode(QFileDialog::AcceptOpen);
    if (fd.exec())
    {
        QString filename=fd.selectedFiles().first();
        if (QFile::exists(filename))
        {
            // if user chose file name and it is exists, send it to begin dump restore process
            emit dumpRestoreFileName(filename);
        }
    }
}

void MainWindow::afterConstructor()
{
    // initialize database
    db=new CDatabase(settings->getDBType());
    connect(db,SIGNAL(errorOpenDB(const QString&)),SLOT(databaseError(const QString&)));
    connect(db,SIGNAL(errorDump(const QString&)),SLOT(databaseError(const QString&)));
    connect(db,SIGNAL(errorRestore(const QString&)),SLOT(databaseError(const QString&)));
    connect(db,SIGNAL(functionNotImplemented()),this,SLOT(notImplemented()));
    db->setDatabaseName(settings->getDBName());
    if (!db->open(settings->getDBUser(),settings->getDBPassword()))
        return;
    // initialize model for SQL table
    ui->tableview->setModel(db->getSqlTableModel());
    // to prevent selection of multiple cells
    ui->tableview->setSelectionMode(QAbstractItemView::SingleSelection);
    // set delegate for all columns
    int column_count=db->getColumnsCount();
    for (int i=0;i<column_count;i++)
    {
        CColumnDelegate::INDEXES ind;
        const QString column_name=db->getColumnName(i);
        if (column_name=="num")
        {
            ind=CColumnDelegate::NumIndex;
        }
        else if (column_name=="content")
        {
            ind=CColumnDelegate::ContentIndex;
        }
        else if (column_name=="comment")
        {
            ind=CColumnDelegate::CommentIndex;
        }
        else if (column_name=="full")
        {
            ind=CColumnDelegate::FullIndex;
        }
        else
        {
            ind=CColumnDelegate::ErrorIndex;
        }
        ui->tableview->setItemDelegateForColumn(i,new CColumnDelegate(this,ind));
    }
    // connect button and menu signals to apropriate slots
    connect(ui->filterButton,SIGNAL(clicked(bool)),SLOT(filtering(bool)));
    connect(ui->addButton,SIGNAL(clicked()),SLOT(addRecord()));
    connect(ui->removeButton,SIGNAL(clicked()),SLOT(deleteRecord()));
    connect(ui->about_qt,SIGNAL(triggered()),SLOT(aboutQt()));
    connect(ui->about_program,SIGNAL(triggered()),SLOT(aboutProgram()));
    // database dump signal/slot interaction
    connect(this,SIGNAL(dumpSaveFileName(const QString&)),db,SLOT(prepareDatabaseDump(const QString&)));
    connect(ui->sql_dump,SIGNAL(triggered()),SLOT(executeDumpSaveDialog()));
    // database restore signal/slot interaction
    connect(this,SIGNAL(dumpRestoreFileName(const QString&)),db,SLOT(databaseRestore(const QString&)));
    connect(ui->sql_restore,SIGNAL(triggered()),SLOT(executeDumpRestoreDialog()));
    // set fixed column width
    ui->tableview->setColumnWidth(0,40);
    ui->tableview->setColumnWidth(1,500);
    ui->tableview->setColumnWidth(2,300);
    ui->tableview->setColumnWidth(3,40);
    // properly show window
    show();
    ui->tableview->resizeRowsToContents();
}

void MainWindow::databaseError(const QString& errorMessage)
{
    // method, which call when database error occurs :-)
    QMessageBox::critical(this,tr("Error"),errorMessage);
    close();
}

void MainWindow::notImplemented()
{
    QMessageBox::information(this,tr("Information"),tr("This function is not implemented yet"));
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle(tr("QtDiscs"));
    setAttribute(Qt::WA_DeleteOnClose);
    settings=new CSettings();
    // additional initialization tricks, which should be done after object's construction
    connect(this,SIGNAL(mainWindowInitialized()),SLOT(afterConstructor()));
    emit mainWindowInitialized();
}

MainWindow::~MainWindow()
{    
    delete db;
    delete ui;
    delete settings;
}
