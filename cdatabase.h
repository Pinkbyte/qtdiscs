#ifndef CDATABASE_H
#define CDATABASE_H

#include <QObject>
#include <QString>
#include <QSqlDatabase>
#include <QSqlQuery>

class QSqlTableModel;

class CDatabase : public QObject
{
    Q_OBJECT
public:
    CDatabase(QString dbtype);
    ~CDatabase();
    void setDatabaseName(const QString& name);
    bool open(const QString& user=QString(),const QString& password=QString());
    QSqlTableModel* getSqlTableModel();
    bool insertRowToModel();
    bool removeRowFromModel(int num);
    void setModelFilter(const QString& filterPredicate=QString());
    int getColumnsCount();
    const QString getColumnName(int index);
signals:
    void errorOpenDB(const QString& errorMessage);
    void errorDump(const QString& errorMessage);
    void errorRestore(const QString& errorMessage);
    void functionNotImplemented();
public slots:
    void prepareDatabaseDump(const QString& filename);
    void databaseRestore(const QString& filename);
private:
    QSqlDatabase* db;
    QSqlTableModel* model;
};

#endif // CDATABASE_H
