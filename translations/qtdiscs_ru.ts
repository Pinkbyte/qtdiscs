<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>CDatabase</name>
    <message>
        <location filename="cdatabase.cpp" line="55"/>
        <source>Could not open database %1</source>
        <translation>Невозможно открыть базу данных %1</translation>
    </message>
    <message>
        <location filename="cdatabase.cpp" line="61"/>
        <source>Database %1 is corrupted or has no tables</source>
        <translation>База данных %1 повреждена или не имеет ни одной таблицы</translation>
    </message>
    <message>
        <location filename="cdatabase.cpp" line="164"/>
        <source>Error while database dump: unknown column type, can not backup</source>
        <translation>Произошла ошибка при попытке дампа базы данных: тип значения не известен, продолжение невозможно</translation>
    </message>
    <message>
        <location filename="cdatabase.cpp" line="185"/>
        <source>I/O Error while database dump: can not write dump file</source>
        <translation>Ошибка ввода-вывода :невозможно записать файл дампа</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <location filename="mainwindow.cpp" line="64"/>
        <location filename="mainwindow.cpp" line="177"/>
        <source>QtDiscs</source>
        <translation>QtDiscs</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="42"/>
        <source>Filter:</source>
        <translation>Фильтр:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="52"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="89"/>
        <source>Add record</source>
        <translation>Добавить запись</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="96"/>
        <source>Delete record</source>
        <translation>Удалить запись</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="116"/>
        <source>Menu</source>
        <translation>Меню</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="136"/>
        <source>About Qt...</source>
        <translation>О Qt...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="141"/>
        <source>About program...</source>
        <translation>О программе...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="146"/>
        <source>SQL dump</source>
        <translation>Дамп базы</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="151"/>
        <source>SQL restore</source>
        <translation>Восстановить дамп</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="60"/>
        <source>QtDiscs</source>
        <comment>program name</comment>
        <translation>Программа QtDiscs</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="61"/>
        <source>Author: Sergey Popov(Pinkbyte)</source>
        <translation>Автор: Попов Сергей(Pinkbyte)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="62"/>
        <source>Compiled with Qt %1</source>
        <translation>Скомпилирована с Qt %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="63"/>
        <source>Running on Qt %1</source>
        <translation>Запущена на Qt %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="73"/>
        <source>Save SQL dump</source>
        <translation>Сохранить дамп базы</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="43"/>
        <location filename="mainwindow.cpp" line="170"/>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="84"/>
        <source>Restore SQL dump</source>
        <translation>Восстановить дамп базы</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="170"/>
        <source>This function is not implemented yet</source>
        <translation>Данная функция еще не реализована</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="164"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
</context>
</TS>
