#include <QApplication>
#include <QLibraryInfo>
#include <QTranslator>
#include <QLocale>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    // get system locale
    QString locale = QLocale::system().name();
    // set translation for internal dialog windows
    QTranslator base_translator;
    base_translator.load(QString("qt_") + locale, QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    a.installTranslator(&base_translator);
    // set translation for application forms both for in-source run and clear install
    QTranslator translator;
    if(!(translator.load(QString("qtdiscs_") + locale, DATA_INSTALL_PATH)))
       translator.load(QString("qtdiscs_") + locale, "translations");
    a.installTranslator(&translator);
    // at last - create main window
    MainWindow* w=new MainWindow();
    QObject::connect(w,SIGNAL(destroyed()),&a,SLOT(quit()));
    return a.exec();
}
