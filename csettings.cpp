#include "csettings.h"

CSettings::CSettings() :
    QSettings(QSettings::UserScope,"qtdiscs")
{
}

QString CSettings::getDBType()
{
    const QString& dbtype=value("dbtype","sqlite").toString();
    QString dbtype_arg; // variable, that would be returned as database type
    if (dbtype=="sqlite")
        dbtype_arg="QSQLITE";
    else
        dbtype_arg="UNKNOWN";
    return dbtype_arg;
}

QString CSettings::getDBName()
{
    return value("dbname","discs.sqlite").toString();
}

QString CSettings::getDBHost()
{
    return value("dbhost","localhost").toString();
}

QString CSettings::getDBUser()
{
    return value("dbuser","discs").toString();
}

QString CSettings::getDBPassword()
{
    return value("dbpass","discs").toString();
}
