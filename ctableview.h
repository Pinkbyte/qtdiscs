#ifndef CTABLEVIEW_H
#define CTABLEVIEW_H

#include <QTableView>

class CTableView : public QTableView
{
    Q_OBJECT
public:
    explicit CTableView(QWidget *parent = 0);

protected:
    virtual void commitData(QWidget* editor);

};

#endif // CTABLEVIEW_H
