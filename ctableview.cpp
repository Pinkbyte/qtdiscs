#include "ctableview.h"

CTableView::CTableView(QWidget *parent) :
    QTableView(parent)
{
}

void CTableView::commitData(QWidget * editor)
{
    QTableView::commitData(editor);
    model()->submit();
    resizeRowsToContents();
}

