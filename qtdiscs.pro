#-------------------------------------------------
#
# Project created by QtCreator 2010-04-24T14:02:06
#
#-------------------------------------------------

QT       += sql widgets

TARGET = qtdiscs
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ctableview.cpp \
    ccolumndelegate.cpp \
    csettings.cpp \
    cdatabase.cpp

HEADERS  += mainwindow.h \
    ctableview.h \
    ccolumndelegate.h \
    csettings.h \
    cdatabase.h

FORMS    += mainwindow.ui

OTHER_FILES +=

RESOURCES += \
    qtdiscs.qrc

TRANSLATIONS = translations/qtdiscs_ru.ts

CODECFORTR = UTF-8

isEmpty(DATA_INSTALL_PATH):DATA_INSTALL_PATH = /usr/share/qtdiscs
DEFINES += DATA_INSTALL_PATH=\\\"$${DATA_INSTALL_PATH}\\\"

data.files = translations/*.qm
data.path = $${DATA_INSTALL_PATH}

installfiles.files += qtdiscs
installfiles.path = $$[QT_INSTALL_BINS]
INSTALLS += installfiles

!isEmpty(TRANSLATIONS) {
   isEmpty(QMAKE_LRELEASE) {
     win32:QMAKE_LRELEASE = $$[QT_INSTALL_BINS]\lrelease.exe
     else:QMAKE_LRELEASE = $$[QT_INSTALL_BINS]/lrelease
   }

   TSQM.name = lrelease ${QMAKE_FILE_IN}
   TSQM.input = TRANSLATIONS
   TSQM.output = ${QMAKE_FILE_BASE}.qm
   TSQM.commands = $$QMAKE_LRELEASE ${QMAKE_FILE_IN}
   TSQM.CONFIG = no_link
   QMAKE_EXTRA_COMPILERS += TSQM
   PRE_TARGETDEPS += compiler_TSQM_make_all
}
else:message(No translation files in project)
