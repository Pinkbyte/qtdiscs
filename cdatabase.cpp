#include "cdatabase.h"

#include <QSqlRecord>
#include <QSqlField>
#include <QSqlError>
#include <QSqlTableModel>
#include <QFile>
#include <QTextStream>
#include <QStringList>

CDatabase::CDatabase(QString dbtype)
{
    db=new QSqlDatabase();
    *db = QSqlDatabase::addDatabase(dbtype);
    model = 0;
}

CDatabase::~CDatabase()
{
    if (model!=0)
        delete model;
    delete db;
}

bool CDatabase::insertRowToModel()
{
    return model->insertRow(model->rowCount());
}

bool CDatabase::removeRowFromModel(int num)
{
    return model->removeRow(num);
}

void CDatabase::setModelFilter(const QString& filterPredicate)
{
    return model->setFilter(filterPredicate);
}

int CDatabase::getColumnsCount()
{
    return model->columnCount();
}

const QString CDatabase::getColumnName(int index)
{
    return model->headerData(index,Qt::Horizontal).toString();
}

bool CDatabase::open(const QString& user,const QString& password)
{
    // check for generic DB open errors
    if (!db->open(user,password))
    {
        emit errorOpenDB(QString(tr("Could not open database %1")).arg(db->databaseName()));
        return false;
    }
    // additional sqlite check: exit if there is no tables in DB
    if (!db->tables().count())
    {
        emit errorOpenDB(QString(tr("Database %1 is corrupted or has no tables")).arg(db->databaseName()));
        return false;
    }
    model = new QSqlTableModel(0,*db);
    model->setTable("discs");
    model->setEditStrategy(QSqlTableModel::OnFieldChange);
    model->select();
    return true;
}

QSqlTableModel* CDatabase::getSqlTableModel()
{
    return model;
}

void CDatabase::setDatabaseName(const QString& name)
{
    return db->setDatabaseName(name);
}

void CDatabase::databaseRestore(const QString& filename)
{
    QFile file(filename);
    bool result=file.open(QIODevice::ReadOnly | QIODevice::Text);
    if (!result)
    {
        // STUB: dump can not be read! need to call some signal...
        return;
    }
    QTextStream in(&file);
    QSqlQuery query(*db);
    bool errors=false;
    if (db->driverName()=="QSQLITE")
    {
        // read dump from file
        QStringList querieslist;
        // get all queries as list of strings
        querieslist=in.readAll().split(";\n");
        QString query_text;
        // quick and dirty implementation of database dump restore.
        // STUBs must be eliminated!
        foreach(query_text,querieslist)
        {
            // skip empty queries
            if (query_text.isEmpty())
                continue;
            query.exec(query_text);
            if (query.lastError().type()!=QSqlError::NoError)
            {
                errors=true;
                query.exec("ROLLBACK");
                break;
            }
        }
    }
    else
    {
        emit functionNotImplemented();
    }
    file.close();
}

void CDatabase::prepareDatabaseDump(const QString& filename)
{
    QSqlQuery query(*db);
    if (db->driverName()=="QSQLITE")
    {
        // select all table names and create queries for backup
        query.exec("SELECT name,sql FROM sqlite_master WHERE type='table' AND name NOT LIKE 'sqlite_%'");
        QStringList table_names; // array of table names
        QStringList create_table_queries; // array of create table queries, one per table
        QString backuptext=QString("BEGIN TRANSACTION;")+"\n";
        while(query.next())
        {
            table_names<<query.record().value(0).toString();
            create_table_queries<<query.record().value(1).toString();
        }
        backuptext+=create_table_queries.join("\n")+";\n";
        query.clear();
        foreach(QString table,table_names)
        {
            query.exec("SELECT * FROM "+table);
            int columns_count=query.record().count(); // columns count in table
            while(query.next())
            {
                QString tmp="INSERT INTO \""+table+"\" VALUES("; // temp variable for backup one row
                for (int i=0;i<columns_count;i++)
                {
                    switch(query.record().field(i).type())
                    {
                        case QVariant::String:
                        {
                            // replace is needed for proper escaping "'" symbol
                            tmp+="'"+QString(query.record().field(i).value().toString()).replace("'","''")+"'";
                            break;
                        }
                        case QVariant::Int:
                        {
                            tmp+=query.record().field(i).value().toString();
                            break;
                        }
                        default:
                        {
                            emit errorDump(tr("Error while database dump: unknown column type, can not backup"));
                            return;
                        }
                    }
                    // add column separator, except for last column
                    if (i!=columns_count-1)
                        tmp+=",";
                }
                tmp+=");";
                backuptext+=tmp+"\n";
            }
            query.clear();
        }
        backuptext+=QString("COMMIT;")+"\n";
        // dump ready, now write it to file
        // write dump to file
        QFile file(filename);
        bool result=file.open(QIODevice::ReadWrite | QIODevice::Text);
        if (!result)
        {
            // dump can not be written!
            emit errorDump(tr("I/O Error while database dump: can not write dump file"));
            return;
        }
        // write dump to file
        QTextStream out(&file);
        out<<backuptext;
        file.close();
    }
    else
    {
        emit functionNotImplemented();
    }
}
